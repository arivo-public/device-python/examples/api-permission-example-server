from datetime import datetime
from enum import Enum
from typing import Optional

from pydantic import BaseModel, Field


class AccessCategory(str, Enum):
    FILLER = "filler"
    PERMANENT = "permanent"
    BOOKED = "booked"


class Direction(str, Enum):
    UNKNOWN = ""
    IN = "in"
    OUT = "out"


class Gate(BaseModel):
    gate_id: str = ""
    direction: Direction = ""


class MediumType(str, Enum):
    LPR = "lpr"
    NFC = "nfc"
    PIN = "pin"
    QR = "qr"


class AccessPermission(BaseModel):
    user_id: str = Field(example="john-doe", description="A unique string identifying the user.")
    category: AccessCategory = Field(example=AccessCategory.PERMANENT, description="Defines type of access.")
    start: datetime = Field(example=datetime(2020, 1, 1, 15), description="Start of access permission in UTC.")
    end: Optional[datetime] = Field(example=datetime(2021, 2, 1, 16),
                                    description="An optional end to the access in UTC.")
    duration: Optional[int] = Field(example=21600,
                                    description="The optional duration in seconds. Is only valid if ```recurrence``` is set.")
    recurrence: Optional[str] = Field(example="DTSTART:19990101T120000Z\nRRULE:FREQ=WEEKLY;WKST=MO;BYDAY=MO,TU,WE,TH,FR",
                                      description="Optional [rrule string](https://jakubroztocil.github.io/rrule/), e.g. access permission only valid on every monday.")
    zone_id: Optional[str] = Field(example="zone1",
                                   description="A unique string identifying the zone the access permission is valid for.")
    check_door_access: Optional[bool] = Field(False,
                                              description="Specifies if a user is allowed to access doors located in the parking lot, if the vehicle is not checked in.")


class AccessRequest(BaseModel):
    gate: Optional[Gate] = Field(None, example=Gate(gate_id="gate1", direction="in"),
                                 description="The gate from where the request to access was sent.")
    medium_type: MediumType = Field(example=MediumType.LPR,
                                    description="Which kind of medium triggered the AccessRequest.")
    medium_id: str = Field(example="GJOHN1",
                           description="The unique id of the medium, e.g. license plate if medium_type is ```lpr```")
    resource: str = Field(example="garage1", description="The unique identifier of the garage.")


class Passage(BaseModel):
    passage_id: str = Field(example="448dd3d0-9574-4b2e-abb5-b932c7a05019",
                            description="A unique string for the current passage process.")
    user_id: str = Field(example="john-doe", description="A unique string identifying the user.")
    resource: str = Field(example="garage1", description="The unique identifier of the garage.")
    gate: Gate = Field(example=Gate(gate_id="gate1", direction="in"), description="The gate to pass through.")
    medium_type: MediumType = Field(example=MediumType.LPR, description="Which kind of medium triggered the passage.")
    medium_id: str = Field(example="GJOHN1",
                           description="The unique id of the medium, e.g. license plate if medium_type is ```lpr```.")
    count: int = Field(example="42", description="The current occupancy count.")
    zone_id: str = Field(example="zone1",
                         description="A unique string identifying the zone the access permission is valid for.")
    category: AccessCategory = Field(example=AccessCategory.PERMANENT, description="Defines type of access.")
    correction: bool = Field(False,
                             description="Determines if it is an actual passage or a correction of a previous one.")
    error: Optional[str] = Field(description="An optional error, e.g. if the passage was a double entry/exit.")
