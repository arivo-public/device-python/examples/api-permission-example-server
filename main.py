import os
import random
import secrets
from datetime import datetime, timedelta

from dotenv import load_dotenv
from fastapi import FastAPI, Depends, HTTPException, status, Request
from fastapi.responses import HTMLResponse
from fastapi.security import HTTPBasic, HTTPBasicCredentials
from fastapi.staticfiles import StaticFiles
from fastapi.templating import Jinja2Templates

from models import AccessRequest, Passage, AccessPermission, AccessCategory

load_dotenv()

app = FastAPI()
app.mount("/static", StaticFiles(directory="static"), name="static")

templates = Jinja2Templates(directory="templates")
http_basic = HTTPBasic()


def create_mock_access_permission_for_user(user, valid=True):
    end = datetime.now() + timedelta(days=100) if valid else datetime(2021, 2, 2)
    return AccessPermission(
        user_id=user,
        category=random.choice(list(AccessCategory)),
        start=datetime(2021, 1, 1),
        end=end,
        zone=random.choice(["zone1", "zone2", "zone3"])
    )


mock_users = {
    "john-doe": {
        "vehicles": ["GJOHN1", "GJOHN2", "GJOHN3"],
        "access_permissions": [
            create_mock_access_permission_for_user('john-doe'),
            create_mock_access_permission_for_user('john-doe')
        ]
    },
    "tom-foo": {
        "vehicles": ["GDOMI1"],
        "access_permissions": [
            create_mock_access_permission_for_user('tom-foo'),
            create_mock_access_permission_for_user('tom-foo')
        ]
    },
    "ron-bar": {
        "vehicles": ["GRON1", "GRON2"],
        "access_permissions": [
            create_mock_access_permission_for_user('ron-bar'),
            create_mock_access_permission_for_user('ron-bar')
        ]
    },
    "don-spam": {
        "vehicles": ["GDON1", "GDON2"],
        "access_permissions": [
            create_mock_access_permission_for_user('don-spam'),
            create_mock_access_permission_for_user('don-spam')
        ]
    },
    "kevin": {
        "vehicles": ["GKEVIN1"],
        "access_permissions": [
            create_mock_access_permission_for_user('kevin'),
            create_mock_access_permission_for_user('kevin')
        ]
    },
    "philipp": {
        "vehicles": ["GPHIL1"],
        "access_permissions": [
            create_mock_access_permission_for_user('philipp', False),
            create_mock_access_permission_for_user('philipp', False)
        ]
    }
}

checked_in = list()


def authorize_basic(credentials: HTTPBasicCredentials = Depends(http_basic)):
    """
    Authorization via HTTPBasic
    """
    correct_username = secrets.compare_digest(credentials.username, os.environ.get("user"))
    correct_password = secrets.compare_digest(credentials.password, os.environ.get("password"))
    if not (correct_username and correct_password):
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Incorrect user or password",
            headers={"WWW-Authenticate": "Basic"},
        )


@app.get("/", response_class=HTMLResponse, include_in_schema=False)
def read_root(request: Request):
    """
    This endpoint only serves as a visualization for the checked in vehicles
    """
    return templates.TemplateResponse("index.html", {"request": request, "checked_in": checked_in})


@app.post("/check_access/", dependencies=[Depends(authorize_basic)])
def check_access(payload: AccessRequest):
    """
    Retrieve access permissions for user

    Returns a list of AccessPermissions for the user the medium belongs to
    """
    users = [key for key, value in mock_users.items() if payload.medium_id in value['vehicles']]
    if users:
        access_permissions = mock_users[users[0]]['access_permissions']
    else:
        access_permissions = []
    return {"access_permissions": access_permissions}


@app.post("/check_in/", dependencies=[Depends(authorize_basic)])
def check_in(payload: Passage):
    """
    Checks in a vehicle

    Returns 200 on success
    """

    checked_in.append(payload.medium_id)

    return status.HTTP_200_OK


@app.post("/check_out/", dependencies=[Depends(authorize_basic)])
def check_out(payload: Passage):
    """
    Checks out a vehicle

    Returns 200 on success
    """
    checked_in.remove(payload.medium_id)

    return status.HTTP_200_OK
