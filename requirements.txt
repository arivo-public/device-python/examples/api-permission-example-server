fastapi
uvicorn==0.13.4
jinja2
aiofiles==0.6.0
python-dotenv==0.15.0
